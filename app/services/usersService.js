const usersRepository = require("../repositories/usersRepository");

module.exports = {
    async create(requestBody) {
        return usersRepository.create(requestBody);
    },

    async update(id, requestBody) {
        return usersRepository.update(id, requestBody);
    },

    async delete(id) {
        return usersRepository.delete(id);
    },

    async get(id) {
        return usersRepository.find(id);
    },

    async list() {
        try {
            const type_users = await usersRepository.findAll();

            return {
                data: type_users,
            };
        } catch (err) {
            throw err;
        }
    },
};
