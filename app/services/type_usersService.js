const type_usersRepository = require("../repositories/type_usersRepository");

module.exports = {
    create(requestBody) {
        return type_usersRepository.create(requestBody);
    },

    update(id, requestBody) {
        return type_usersRepository.update(id, requestBody);
    },

    delete(id) {
        return type_usersRepository.delete(id);
    },

    get(id) {
        return type_usersRepository.find(id);
    },

    async list() {
        try {
            const type_users = await type_usersRepository.findAll();

            return {
                data: type_users,
            };
        } catch (err) {
            throw err;
        }
    },

    get(id) {
        return type_usersRepository.find(id);
    },
};
