const type_usersController = require("./type_usersController");
const usersController = require("./usersController");
const type_carsController = require("./type_carsController");
const carsController = require("./carsController");
const authController = require("./authController");
const docsController = require("./docsController");

module.exports = {
    type_usersController,
    type_carsController,
    carsController,
    authController,
    usersController,
    docsController,
};
